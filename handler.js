'use strict';

module.exports = {
    logarithm(event, context){
        return Math.log10(event.data.value);
    },
    sine(event, context){
        return Math.sin(event.data.value);
    },
    cosine(event, context){
        return Math.cos(event.data.value);
    },
    tangent(event, context){
        return Math.tan(event.data.value);
    }
};