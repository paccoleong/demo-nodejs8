import {logarithm, sine, cosine, tangent} from '../handler';

test("test logarithm", () => {
    let test_payload = {'data' : {'value': 5000}};
    let result = logarithm(test_payload);
    expect(parseFloat(result.toFixed(5))).toEqual(3.69897);

    test_payload = {'data' : {'value': 9371298}};
    result = logarithm(test_payload);
    expect(parseFloat(result.toFixed(5))).toEqual(6.97180);
});

test("test sine", () => {
    let test_payload = {'data' : {'value': 5000}};
    let result = sine(test_payload);
    expect(parseFloat(result.toFixed(5))).toEqual(-0.98797);

    test_payload = {'data' : {'value': 9371298}};
    result = sine(test_payload);
    expect(parseFloat(result.toFixed(5))).toEqual(0.58836);
});

test("test cosine", () => {
    let test_payload = {'data' : {'value': 5000}};
    let result = cosine(test_payload);
    expect(parseFloat(result.toFixed(5))).toEqual(0.15467);

    test_payload = {'data' : {'value': 9371298}};
    result = cosine(test_payload);
    expect(parseFloat(result.toFixed(5))).toEqual(-0.80860);
});

test("test tangent", () => {
    let test_payload = {'data' : {'value': 5000}};
    let result = tangent(test_payload);
    expect(parseFloat(result.toFixed(5))).toEqual(-6.38764);

    test_payload = {'data' : {'value': 9371298}};
    result = tangent(test_payload);
    expect(parseFloat(result.toFixed(5))).toEqual(-0.72763);
});